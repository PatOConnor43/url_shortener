defmodule UrlShortener.MixProject do
  use Mix.Project

  def project do
    [
      app: :url_shortener,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test,
        "coveralls.json": :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :postgrex, :ecto],
      mod: {UrlShortener.Application, []},
      applications: [:cowboy, :plug]
    ]
  end

  defp aliases do
    [
      test: "test --no-start" # Prevents the need for postgres to run
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy, "~> 2.0"},
      {:plug, "~> 1.0"},
      {:poison, "~> 3.1"},
      {:uuid, "~> 1.1"},
      {:ecto, "~> 2.0"},
      {:postgrex, "~> 0.13"},
      {:excoveralls, "~> 0.10", only: :test},
      {:distillery, "~> 2.0"}
    ]
  end
end
