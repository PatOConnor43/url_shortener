defmodule UrlShortener.Repo.Migrations.CreateUrls do
  use Ecto.Migration

  def change do
    create table(:url) do
      add :short_url, :string
      add :long_url, :string
      timestamps()
    end
  end
end
