defmodule UrlShortener.Repo.Migrations.SampleUrls do
  use Ecto.Migration
  alias UrlShortener.Repo
  alias UrlShortener.Schema

  def change do
    Repo.insert(%Schema.Url{long_url: "https://google.com", short_url: "876c427"})
    Repo.insert(%Schema.Url{long_url: "https://elixir-lang.org", short_url: "5a82417"})
    Repo.insert(%Schema.Url{long_url: "https://github.com", short_url: "014045e"})
    Repo.insert(%Schema.Url{long_url: "https://hexdocs.pm", short_url: "951d4d9"})
    Repo.insert(%Schema.Url{long_url: "https://gitlab.com", short_url: "2e14465"})
  end
end
