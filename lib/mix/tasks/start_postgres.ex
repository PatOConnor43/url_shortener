defmodule Mix.Tasks.StartPostgres do
  use Mix.Task

  @shortdoc "Start the Postgres database"
  def run(_) do
    {result, code} = System.cmd("docker", ["run", "-p", "5432:5432", "--name", "postgres", "-e", "POSTGRES_PASSWORD=root", "-e", "POSTGRES_USER=url_shortener_repo", "-d", "postgres"])
    case {result, code} do
      {_, c} when c > 0 -> IO.puts "Failed: #{c}"
      {_, _} -> IO.puts "Started sucessfully"
    end
  end
end
