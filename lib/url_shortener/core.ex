defmodule UrlShortener.Core do
  require Logger
  alias UrlShortener.Schema.Url
  import Ecto.Query, only: [from: 2]

  @spec shorten(String.t, UrlShortener.Repo) :: {:ok, String.t}
  def shorten(url, repo \\ UrlShortener.Repo) do

    record = case repo.get_by(UrlShortener.Schema.Url, long_url: url) do
      nil -> create_new_url(url)
      existing when existing != nil -> existing
    end
    IO.inspect record

    {:ok, record}
  end

  defp create_new_url(_url) do
    slice =
      UUID.uuid4(:hex)
      |> String.slice(8..14)

    Logger.info(~s(Generated id: #{slice}))
  end

  @spec get_url_by_short_url(any()) :: {:error, :not_exists} | {:ok, Url}
  def get_url_by_short_url(short_url) do
    query = from url in Url, where: url.short_url == ^short_url
    url = UrlShortener.Repo.one(query)
    cond do
      !is_nil(url) -> {:ok, url}
      true -> {:error, :not_exists}
    end
  end

  def health() do
    query = from url in Url, limit: 1
    url = UrlShortener.Repo.one(query)
    cond do
      !is_nil(url) -> {:ok, url}
      true -> {:error, :not_exists}
    end
  end
end
