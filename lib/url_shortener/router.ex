defmodule UrlShortener.Router do
  use Plug.Router
  use Plug.Debugger
  require Logger

  plug(Plug.Logger, log: :debug)

  plug(:match)
  plug(:dispatch)

  forward("/v1", to: UrlShortener.V1.Router)

  get "/health" do
    with {:ok, _} <- UrlShortener.Core.health() do
      send_resp(conn, 200, "OK")
    else
      {:error, _} -> send_resp(conn, 500, "BAD")
    end
  end

  match _ do
    IO.inspect(conn.path_info)
    id = List.last(conn.path_info)
    Logger.info(~s(Looking up url for #{id}))
    with {:ok, url} <- UrlShortener.Core.get_url_by_short_url(id) do
      conn = Plug.Conn.put_resp_header(conn, "location", url.long_url)
      Logger.info(~s(Found #{url.long_url} for #{id}))
      send_resp(conn, 301, url.long_url)
    else
      {:error, _} ->
        Logger.info(~s(Could not find url for #{id}))
        send_resp(conn, 404, "Not Found")
    end
  end
end
