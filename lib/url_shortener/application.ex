defmodule UrlShortener.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: UrlShortener.Worker.start_link(arg)
      # {UrlShortener.Worker, arg},
      worker(UrlShortener.Repo, []),
      {Plug.Adapters.Cowboy2, scheme: :http, plug: UrlShortener.Router, options: [port: 4000]},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: UrlShortener.Supervisor]

    # Plug.Adapters.Cowboy2.child_spec(scheme: :http, plug: UrlShortener.Router, opts: [port: 4000])
    Supervisor.start_link(children, opts)
  end
end
