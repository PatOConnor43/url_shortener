defmodule UrlShortener.V1.ShortenPost do
  require Logger
  alias UrlShortener.Core
  @behaviour UrlShortener.V1.Handler

  @spec handle(map()) :: {:ok, String.t} | {:error, String.t}
  def handle(%{"long_url" => url} = _data) do
    with {:ok, link} <- Core.shorten(url),
         {:ok, body} <- Poison.encode(%{link: link}) do
      {:ok, body}
    else
      {:error, reason} ->
        Logger.error(reason)
        {:error, reason}
    end
  end

  def handle(data) do
    Logger.debug(~s(Received non-matching data #{data}))
    {:error, :bad_keys}
  end

end
