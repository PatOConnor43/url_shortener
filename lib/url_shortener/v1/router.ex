defmodule UrlShortener.V1.Router do
  use Plug.Router
  use Plug.Debugger
  require Logger

  plug(Plug.Logger, log: :debug)

  plug(:match)
  plug(:dispatch)

  post "/shorten" do
    handle(conn, &UrlShortener.V1.ShortenPost.handle/1)
  end

  match _ do
    send_resp(conn, 404, "Not Found")
  end

  @spec handle(Plug.Conn.t(), (map() -> String.t())) :: Plug.Conn.t()
  def handle(conn, handler) do
    {:ok, body, conn} = read_body(conn)

    with {:ok, body} <- Poison.decode(body),
         {:ok, body} <- handler.(body) do
      send_resp(conn, 200, body)
    else
      {:error, :invalid} -> send_resp(conn, 400, "Bad Request")
      {:error, _} -> send_resp(conn, 500, "Internal Error")
    end
  end
end
