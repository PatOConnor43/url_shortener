defmodule UrlShortener.V1.Handler do
  @callback handle(map()) :: {:ok, String.t} | {:error, String.t}
end
