defmodule UrlShortener.Schema.Url do
  use Ecto.Schema

  schema "url" do
    field :short_url, :string
    field :long_url, :string
    timestamps()
  end
end
