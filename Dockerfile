FROM bitwalker/alpine-elixir:1.7.4

# Set exposed ports
EXPOSE 4000
ENV PORT=4000

ENV MIX_ENV=prod

COPY app.tar.gz app.tar.gz
RUN tar -xzvf app.tar.gz

USER default

CMD ./bin/url_shortener foreground