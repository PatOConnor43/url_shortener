defmodule Test.UrlShortener.Core do
  use ExUnit.Case
  alias UrlShortener.Core
  doctest UrlShortener.Core

  test "greets the world" do
    url = %UrlShortener.Schema.Url{short_url: "short", long_url: "long"}
    defmodule MockRepo do
      def get_by(_, _) do
        %UrlShortener.Schema.Url{short_url: "short", long_url: "long"}
      end
    end
    repo = MockRepo
    assert Core.shorten("long", repo) == {:ok, url}
  end
end
